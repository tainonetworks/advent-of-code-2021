package day4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class GiantSquidAppPart2 {

	public static void main(String[] args) {
		BingoGamePart1 game = new BingoGamePart1();
		Queue<String> calls = new LinkedBlockingQueue<String>();
		try {
			File file = new File("src\\main\\java\\day4\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			
			//init calls
			if((line = br.readLine()) != null) {
				String[] numbers = line.split(",");
				for(String number : numbers) {
					calls.add(number);
				}
			}
			
			BingoBoard board = new BingoBoard();
			int row = 0;
			while ((line = br.readLine()) != null) {
				if(line.isEmpty()) {
					continue;
				}
				if(board == null) {
				  board = new BingoBoard();
				  row = 0;
				}
				
				for(int col = 0; col < 5; col++) {	
					String number = line.substring(0, 2).trim();
					if(line.length() >= 3)
						line = line.substring(3);
					board.addNumber(row, col, number);
				}
				
				row++;
				if(row >= 5) {
					game.addBoard(board);
					board = null;
				}
			}
			fr.close();
			
			while(!calls.isEmpty()) {
				String number = calls.poll();
				game.call(number);
				
				List<BingoBoard> winners = game.getBingos();			
				for(BingoBoard winner : winners) {
					System.out.println("Points: " + (winner.getScore() * Integer.parseInt(number)));
					game.removeBoard(winner);
				}
				
				if(game.getBoards().size() == 0)
					break;
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
