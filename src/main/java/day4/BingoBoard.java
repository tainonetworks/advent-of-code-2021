package day4;

public class BingoBoard {
	
	private String[][] grid = new String[5][5];

	public void addNumber(int row, int col, String value) {
		grid[row][col] = value;
	}
	
	public void call(String number) {
		for(int row = 0; row < 5; row++) {
			for(int col = 0; col < 5; col++) {
				if(grid[row][col] != null && grid[row][col].equals(number)) {
					grid[row][col] = null;
					return;
				}
			}
		}
	}
	
	public boolean hasBingo() {
		horizontal:
		for(int row = 0; row < 5; row++) {
			for(int col = 0; col < 5; col++) {
				if(grid[row][col] != null) {
					continue horizontal;
				}
			}
			return true;
		}
		vertical:
		for(int col = 0; col < 5; col++) {
			for(int row = 0; row < 5; row++) {
				if(grid[row][col] != null) {
					continue vertical;
				}
			}
			return true;
		}
		return false;
	}
	
	public int getScore() {
		int total = 0;
		for(int row = 0; row < 5; row++) {
			for(int col = 0; col < 5; col++) {
				if(grid[row][col] != null)
					total += Integer.parseInt(grid[row][col]);
			}
		}
		return total;
	}
}
