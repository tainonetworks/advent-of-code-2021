package day4;

import java.util.ArrayList;
import java.util.List;

public class BingoGamePart1 {
	
	private List<BingoBoard> boards = new ArrayList<BingoBoard>();
	
	public void addBoard(BingoBoard board) {
		if(board == null) {
			throw new IllegalArgumentException("board cannot be null");
		}
		boards.add(board);
	}
	
	public void removeBoard(BingoBoard board) {
		if(boards.contains(board)) {
			boards.remove(board);
		}
	}
	
	public List<BingoBoard> getBoards() {
		return boards;
	}
	
	public void call(String number) {
		for(BingoBoard board : boards) {
			board.call(number);
		}
	}
	
	public List<BingoBoard> getBingos() {
		
		List<BingoBoard> bingoBoards = new ArrayList<BingoBoard>();
		
		for(BingoBoard board : boards) {
			if(board.hasBingo()) {
				bingoBoards.add(board);
			}
		}
		return bingoBoards;
	}
	

}
