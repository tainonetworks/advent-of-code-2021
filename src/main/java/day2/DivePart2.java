package day2;

public class DivePart2 {

	private int horizontalPosition = 0;
	private int depth = 0;
	private int aim = 0;

	public void move(String command) {
		if (command == null) {
			throw new IllegalArgumentException("command cannot be null");
		}
		String[] values = command.split(" ");
		if (values.length != 2) {
			throw new IllegalArgumentException("command must be the direction follow by the distance");
		}

		String direction = values[0].toLowerCase();
		int distance = Integer.parseInt(values[1]);

		switch (direction) {
			case "forward":
				horizontalPosition += distance;
				depth += (aim * distance);
				break;
			case "down":
				aim += distance;
				break;
			case "up":
				aim -= distance;
				break;
			default:
				throw new IllegalArgumentException("unknown direction");
		}
	}
	
	public int getResult() {
		System.out.println(horizontalPosition);
		System.out.println(depth);
		System.out.println(aim);
		return horizontalPosition * depth;
	}

}
