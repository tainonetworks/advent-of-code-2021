package day2;

public class DivePart1 {

	private int horizontalPosition = 0;
	private int depth = 0;

	public void move(String command) {
		if (command == null) {
			throw new IllegalArgumentException("command cannot be null");
		}
		String[] values = command.split(" ");
		if (values.length != 2) {
			throw new IllegalArgumentException("command must be the direction follow by the distance");
		}

		String direction = values[0].toLowerCase();
		int distance = Integer.parseInt(values[1]);

		switch (direction) {
			case "forward":
				horizontalPosition += distance;
				break;
			case "down":
				depth += distance;
				break;
			case "up":
				depth -= distance;
				break;
			default:
				throw new IllegalArgumentException("unknown direction");
		}
	}
	
	public int getResult() {
		System.out.println(horizontalPosition);
		System.out.println(depth);
		return horizontalPosition * depth;
	}

}
