package day2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class DiveAppPart2 {

	public static void main(String[] args) {
		DivePart2 dive = new DivePart2();
		try {
			File file = new File("src\\main\\java\\day2\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				dive.move(line);
			}
			fr.close();
			System.out.println(dive.getResult());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
