package day16;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PacketDecoderPart1 {
	
	static int versionsAggregate = 0;
	
	static StringBuffer representation = new StringBuffer();

	public static void main(String[] args) {
		Map<Character, String> hexToBinary = new HashMap<Character, String>();
		hexToBinary.put('0', "0000");
		hexToBinary.put('1', "0001");
		hexToBinary.put('2', "0010");
		hexToBinary.put('3', "0011");
		hexToBinary.put('4', "0100");
        hexToBinary.put('5', "0101");
        hexToBinary.put('6', "0110");
        hexToBinary.put('7', "0111");
        hexToBinary.put('8', "1000");
        hexToBinary.put('9', "1001");
        hexToBinary.put('A', "1010");
        hexToBinary.put('B', "1011");
        hexToBinary.put('C', "1100");
        hexToBinary.put('D', "1101");
        hexToBinary.put('E', "1110");
        hexToBinary.put('F', "1111");
		
		try {
			File file = new File("src\\main\\java\\day16\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			StringBuffer message = new StringBuffer();
			for(char c : br.readLine().toCharArray()) {
				message.append(hexToBinary.get(c));
			}
			
			fr.close();			

			System.out.println("Total " + process(message));
			
			System.out.println("Versions aggregate " + versionsAggregate);
			
//			System.out.println(representation);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
	static Long process(StringBuffer message) {
		long value = 0;
		int version = binaryToInt(pull(message, 3));
		versionsAggregate += version;
		int type = binaryToInt(pull(message, 3));
		if(type == 4) {
			value = binaryToLong(getLiteralValue(message));
		} else {//operator
			int mode = binaryToInt(pull(message, 1));
			List<Long> subValues = new ArrayList<Long>();
			if(mode == 0) {
				int bits = binaryToInt(pull(message, 15));
				StringBuffer submessage = pull(message, bits);
				while(submessage.length() > 0) {
					subValues.add(process(submessage));
				}
			} else {
				int packets = binaryToInt(pull(message, 11));
				for(int i = 0; i < packets; i++) {
					subValues.add(process(message));
				}
			}
			if(type == 0) {
				representation.append("sum(");
				for(long subValue : subValues) {
					value += subValue;
				}
			} else if(type == 1) {
				representation.append("mult(");
				value = 1;//primer
				for(long subValue : subValues) {
					value *= subValue;
				}
			} else if(type == 2) {
				representation.append("min(");
				value = Long.MAX_VALUE;//primer
				for(long subValue : subValues) {
					value = Long.min(value, subValue);
				}
			} else if(type == 3) {
				representation.append("max(");
				value = Long.MIN_VALUE;//primer
				for(long subValue : subValues) {
					value = Long.max(value, subValue);
				}
			} else if(type == 5) {
				value = subValues.get(0) > subValues.get(1) ? 1L : 0L;
				representation.append("gt(");
			} else if(type == 6) {
				value = subValues.get(0) < subValues.get(1) ? 1L : 0L;
				representation.append("lt(");
			} else if(type == 7) {
				value = subValues.get(0).equals(subValues.get(1)) ? 1L : 0L;
				representation.append("eq(");
			}
			representation.append(subValues + ") = " + value + "\n");
		}
		return value;
	}
	
	static StringBuffer getLiteralValue(StringBuffer message) {
		StringBuffer literalValue = new StringBuffer();
		while(true) {
			StringBuffer code = pull(message, 5);
			literalValue.append(code.substring(1,5));
			if(code.charAt(0) == '0') {
				break;
			}
		}
		return literalValue;
	}
	
	static StringBuffer pull(StringBuffer message, int amount) {
		StringBuffer subMessage = new StringBuffer(message.substring(0, amount));
		message.delete(0, amount);
		return subMessage;
	}
	
	static int binaryToInt(StringBuffer binary) {
		return Integer.parseInt(binary.toString(), 2); 
	}
	
	static long binaryToLong(StringBuffer binary) {
		return Long.parseLong(binary.toString(), 2); 
	}
}
