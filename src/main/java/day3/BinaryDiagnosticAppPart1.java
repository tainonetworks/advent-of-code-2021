package day3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class BinaryDiagnosticAppPart1 {

	public static void main(String[] args) {
		BinaryDiagnosticPart1 binaryDiagnostic = new BinaryDiagnosticPart1();
		try {
			File file = new File("src\\main\\java\\day3\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				binaryDiagnostic.input(line);
			}
			fr.close();
			System.out.println(binaryDiagnostic.getResult());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
