package day3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BinaryDiagnosticAppPart2 {

	public static void main(String[] args) {
		try {
			List<char[]> lines = new ArrayList<char[]>();
			File file = new File("src\\main\\java\\day3\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				lines.add(line.toCharArray());
			}
			fr.close();
			System.out.println(Integer.parseInt(highest(lines, 0), 2) * Integer.parseInt(lowest(lines, 0), 2));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static String highest(List<char[]> lines, int index) {
		if(lines.size() == 1) {
			return String.valueOf(lines.get(0));
		}
		
		List<char[]> ceros = new ArrayList<char[]>();
		List<char[]> ones = new ArrayList<char[]>();
		
		for(int i = 0; i < lines.size(); i++) {
			char[] line = lines.get(i);
			if(line[index] == '0') {
				ceros.add(line);
			} else {
				ones.add(line);
			}
		}
		
		if(ceros.size() > ones.size()) {
			return highest(ceros, ++index);
		} else {
			return highest(ones, ++index);
		}
	}
	
	static String lowest(List<char[]> lines, int index) {
		if(lines.size() == 1) {
			return String.valueOf(lines.get(0));
		}
		
		List<char[]> ceros = new ArrayList<char[]>();
		List<char[]> ones = new ArrayList<char[]>();
		
		for(int i = 0; i < lines.size(); i++) {
			char[] line = lines.get(i);
			if(line[index] == '0') {
				ceros.add(line);
			} else {
				ones.add(line);
			}
		}
		
		if(ceros.size() <= ones.size()) {
			return lowest(ceros, ++index);
		} else {
			return lowest(ones, ++index);
		}
	}
}
