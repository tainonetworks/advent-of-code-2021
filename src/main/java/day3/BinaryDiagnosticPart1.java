package day3;

public class BinaryDiagnosticPart1 {
	
	private int[] ceroCounts;
	private int[] oneCounts;
	
	public void input(String value) {
		if(value == null) {
			throw new IllegalArgumentException("value cannot be null");
		}
		
		if(!value.matches("[01]{1,}")) {
			throw new IllegalArgumentException("value is not a binary number");
		}
		
		//initialize counters
		if(ceroCounts == null) {
			ceroCounts = new int[value.length()];
			oneCounts = new int[value.length()];
		}
		
		for(int i = 0; i < value.length(); i++) {
			if(value.charAt(i) == '0') {
				ceroCounts[i] += 1;
			} else {
				oneCounts[i] += 1;
			}
		}
	}
	
	public int getResult() {
		StringBuffer gamma = new StringBuffer();
		StringBuffer epsilon = new StringBuffer();
		for(int i = 0; i < ceroCounts.length; i++) {
			if(ceroCounts[i] < oneCounts[i]) {
				gamma.append("1");
				epsilon.append("0");
			} else if (ceroCounts[i] > oneCounts[i]) {
				gamma.append("0");
				epsilon.append("1");
			} else {
				throw new RuntimeException("cero and one counts are the same for bit " + i);
			}
		}
		
		int gamaDecimal = Integer.parseInt(gamma.toString(), 2);
		int epsilonDecimal = Integer.parseInt(epsilon.toString(), 2);
		
		return gamaDecimal * epsilonDecimal;
	}
}
