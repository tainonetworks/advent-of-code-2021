package day15;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChitonPartOld {
	
	static List<int[]> rows = new ArrayList<int[]>();
	static Map<String, Integer> lowestPathValues = new HashMap<String, Integer>();

	public static void main(String[] args) {
		try {
			File file = new File("src\\main\\java\\day15\\demo.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				int[] columns = new int[line.length()];
				for(int i = 0; i < line.length(); i++) {
					columns[i] = Integer.parseInt(line.substring(i,i+1));
				}
				rows.add(columns);
			}
			fr.close();
			
			System.out.println(navigate(0, 0));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static int navigate(int column, int row) {
		String point = column + "," + row;
		int lowestPathValue = 0;
		if(lowestPathValues.containsKey(point)) {
			lowestPathValue = lowestPathValues.get(point);
		} else {
			int[] columns = rows.get(row);
			
			if(row > 0 || column > 0) {
				lowestPathValue += columns[column];
			}
			
			if(row < rows.size()-1 || column < columns.length-1) {
				if(row < rows.size()-1) {
					if(column < columns.length-1) {
						lowestPathValue += Integer.min(navigate(column, row+1), navigate(column+1, row));
					} else {
						lowestPathValue += navigate(column, row+1);
					}
				} else {
					lowestPathValue += navigate(column+1, row);
				}
			}
			lowestPathValues.put(point, lowestPathValue);
		}
		return lowestPathValue;
	}
}
