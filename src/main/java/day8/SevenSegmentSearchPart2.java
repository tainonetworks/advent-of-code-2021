package day8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class SevenSegmentSearchPart2 {

	public static void main(String[] args) {
		try {
			File file = new File("src\\main\\java\\day8\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			int count = 0;
			while ((line = br.readLine()) != null) {
				String[] parts = line.split(" \\| ");
				String[] pattern = parts[0].split(" ");
				for(int i = 0; i < pattern.length; i++) {
					char[] chars = pattern[i].toCharArray();
					Arrays.sort(chars);
					pattern[i] = new String(chars);
				}
				pattern = decodePattern(pattern);
				String[] digits = parts[1].split(" ");
				for(int i = 0; i < digits.length; i++) {
					char[] chars = digits[i].toCharArray();
					Arrays.sort(chars);
					digits[i] = new String(chars);
				}
				count += translate(digits, pattern);
			}
			fr.close();
			System.out.println(count);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String[] decodePattern(String[] pattern) {
		String[] orderedPattern = new String[pattern.length];
		
//		L2 --> V1
//		L3 --> V7
//		L4 --> V4
//		L7 --> V8
		
		for(String code : pattern) {
			if(code.length() == 2) {
				orderedPattern[1] = code;
			} else if(code.length() == 3) {
				orderedPattern[7] = code;
			} else if(code.length() == 4) {
				orderedPattern[4] = code;
			} else if(code.length() == 7) {
				orderedPattern[8] = code;
			}
		}
		for(String code : pattern) {

//			if(L5) --> V2,V3,V5
//			code-V4=L3 --> V2
//			code-V7=L2 --> V3
//			else code --> V5
			
//			if(L6) --> V0,V6,V9
//			code-V7=L4 --> V6
//			code-V4=L2 --> V9
//			else code --> V0
			
			if(code.length() == 5) {
				int count = 0;
				for(int i = 0; i < code.length(); i++) {
					String c = code.substring(i,i+1);
					if(orderedPattern[4].contains(c)) {
						continue;
					}
					count++;
				}
				if(count == 3) {
					orderedPattern[2] = code;
					continue;
				}
				count = 0;
				for(int i = 0; i < code.length(); i++) {
					String c = code.substring(i,i+1);
					if(orderedPattern[7].contains(c)) {
						continue;
					}
					count++;
				}
				if(count == 2) {
					orderedPattern[3] = code;
					continue;
				}
				orderedPattern[5] = code;
			} else if(code.length() == 6) {
				int count = 0;
				for(int i = 0; i < code.length(); i++) {
					String c = code.substring(i,i+1);
					if(orderedPattern[7].contains(c)) {
						continue;
					}
					count++;
				}
				if(count == 4) {
					orderedPattern[6] = code;
					continue;
				}
				count = 0;
				for(int i = 0; i < code.length(); i++) {
					String c = code.substring(i,i+1);
					if(orderedPattern[4].contains(c)) {
						continue;
					}
					count++;
				}
				if(count == 2) {
					orderedPattern[9] = code;
					continue;
				}
				orderedPattern[0] = code;
			}
		}
		return orderedPattern;
	}
	
	public static int translate(String[] digits, String[] pattern) {
		StringBuffer sb = new StringBuffer();
		for(String digit : digits) {
			for(int i = 0; i < pattern.length; i++) {
				if(digit.equals(pattern[i])) {
					sb.append(i);
					break;
				}
			}
		}
		return Integer.parseInt(sb.toString());
	}
}
