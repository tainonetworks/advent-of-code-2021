package day8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class SevenSegmentSearchPart1 {

	public static void main(String[] args) {
		try {
			File file = new File("src\\main\\java\\day8\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			int count = 0;
			while ((line = br.readLine()) != null) {
				String[] parts = line.split(" \\| ");
				String[] digits = parts[1].split(" ");
				
				for(String digit : digits) {
					if(decode(digit)) {
						count++;
					}
				}
			}
			System.out.println(count);
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean decode(String code) {
		if(code.length() == 2 || code.length() == 3 || code.length() == 4 || code.length() == 7) {
			return true;
		}
		return false;
	}
}
