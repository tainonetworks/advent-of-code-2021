package day9;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SmokeBasinPart1 {

	public static void main(String[] args) {
		List<int[]> lines = new ArrayList<int[]>();
		try {
			File file = new File("src\\main\\java\\day9\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				int[] digits = new int[line.length()];
				for(int i = 0; i < line.length(); i++) {
					digits[i] = Integer.parseInt(line.substring(i,i+1));
				}
				lines.add(digits);
			}
			fr.close();
			Map<String, Integer> points = new HashMap<String, Integer>();
			for(int i = 0; i < lines.size(); i++) {
				int[] digits = lines.get(i);
				
				for(int j = 0; j < digits.length; j++) {
					boolean isLowest = true;
					if(j > 0) {
						isLowest = isLowest && digits[j] < digits[j-1];
					}
					if(j < digits.length - 1) {
						isLowest = isLowest && digits[j] < digits[j+1];
					}
					if(i > 0) {
						int[] topDigits = lines.get(i-1);
						isLowest = isLowest && digits[j] < topDigits[j];
					}
					if(i < lines.size() - 1) {
						int[] bottomDigits = lines.get(i+1);
						isLowest = isLowest && digits[j] < bottomDigits[j];
					}
					if(isLowest) {
						points.put(j + "," + i, digits[j]);
					}
				}
			}
			int sum = 0;
			for(int value : points.values()) {
				sum += (value + 1);
			}
			System.out.println(sum);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
