package day9;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SmokeBasinPart2 {
	
	static List<int[]> lines = new ArrayList<int[]>();
	static List<Integer> counts = new ArrayList<Integer>();

	public static void main(String[] args) {
		try {
			File file = new File("src\\main\\java\\day9\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				int[] digits = new int[line.length()];
				for(int i = 0; i < line.length(); i++) {
					digits[i] = Integer.parseInt(line.substring(i,i+1));
				}
				lines.add(digits);
			}
			for(int y = 0; y < lines.size(); y++) {
				int[] digits = lines.get(y);
				for(int x = 0; x < digits.length; x++) {
					int count = count(x, y);
					if(count > 0) {
						counts.add(count);
					}
				}
			}
			Collections.sort(counts);
			System.out.println(counts.get(counts.size()-1) * counts.get(counts.size()-2) * counts.get(counts.size()-3));
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static int count(int x, int y) {
		int count = 0;
		int[] digits = lines.get(y);
		int digit = digits[x];
		
		if(digit != 9) {
			count += 1;
			digits[x] = 9;
			if(x > 0) {
				count += count(x-1, y);
			}
			if(x < digits.length - 1) {
				count += count(x+1, y);
			}
			if(y > 0) {
				count += count(x, y-1);
			}
			if(y < lines.size() - 1) {
				count += count(x, y+1);
			}
			
		}
		return count;
	}
}
