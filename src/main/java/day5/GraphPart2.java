package day5;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GraphPart2 {
	
	Map<String, Integer> map = new HashMap<String, Integer>();
	
	public void addLine(int startX, int startY, int endX, int endY) {	
		
		if(startX != endX) {
			int slope = (endY - startY)/(endX - startX);
			if(slope != 0 && Math.abs(slope) != 1) {
				return;
			}
		}
		
		int currentX = startX;
		int currentY = startY;
		
		while(true) {
			
			String point = currentX + ", " + currentY;
			Integer count = map.get(point);
			if(count == null)
				count = 0;
			map.put(point, ++count);
			
			if(currentX == endX && currentY == endY) {
				break;
			}
			
			if(startX < endX) {
				currentX++;
			} else if(startX > endX) {
				currentX--;
			}
			if(startY < endY) {
				currentY++;
			} else if (startY > endY) {
				currentY--;
			}
		}
	}
	
	public int overlaps() {
		Set<String> keys = map.keySet();
		int overlaps = 0;
		for(String key : keys) {
			Integer value = map.get(key);
			if(value > 1) {
				overlaps++;
			}
		}
		return overlaps;
	}
}
