package day5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class HydrothermalVentureAppPart1 {

	public static void main(String[] args) {
		GraphPart1 graph = new GraphPart1();
		try {
			File file = new File("src\\main\\java\\day5\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] points = line.split(" -> ");
				String[] point1 = points[0].split(",");
				String[] point2 = points[1].split(",");
				int startX = Integer.parseInt(point1[0]);
				int startY = Integer.parseInt(point1[1]);
				int endX = Integer.parseInt(point2[0]);
				int endY = Integer.parseInt(point2[1]);
				graph.addLine(startX, startY, endX, endY);
			}
			fr.close();
			System.out.println(graph.overlaps());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
