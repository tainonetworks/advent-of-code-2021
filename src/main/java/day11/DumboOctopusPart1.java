package day11;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DumboOctopusPart1 {

	static List<int[]> lines = new ArrayList<int[]>();
	static int flashes = 0;
	
	public static void main(String[] args) {
		try {
			File file = new File("src\\main\\java\\day11\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				int[] digits = new int[line.length()];
				for(int i = 0; i < line.length(); i++) {
					digits[i] = Integer.parseInt(line.substring(i,i+1));
				}
				lines.add(digits);
			}
			fr.close();
			
			for(int i = 0; i < 100; i++) {
				for(int y = 0; y < lines.size(); y++) {
					int[] digits = lines.get(y);
					for(int x = 0; x < digits.length; x++) {
						increase(x, y);
					}
				}
				//reset
				for(int y = 0; y < lines.size(); y++) {
					int[] digits = lines.get(y);
					for(int x = 0; x < digits.length; x++) {
						if(digits[x] > 9) {
							digits[x] = 0;
						}
					}
				}
			}
			System.out.println(flashes);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static void increase(int x, int y) {
		int[] digits = lines.get(y);
		int digit = digits[x];
		digit += 1;
		digits[x] = digit;

		
		if(digit == 10) {
			flashes += 1;
			if(x > 0) {
				increase(x-1, y);
			}
			if(x < digits.length - 1) {
				increase(x+1, y);
			}
			if(y > 0) {
				increase(x, y-1);
				if(x > 0) {
					increase(x-1, y-1);
				}
				if(x < digits.length - 1) {
					increase(x+1, y-1);
				}
			}
			if(y < lines.size() - 1) {
				increase(x, y+1);
				if(x > 0) {
					increase(x-1, y+1);
				}
				if(x < digits.length - 1) {
					increase(x+1, y+1);
				}
			}
		}
	}
}
