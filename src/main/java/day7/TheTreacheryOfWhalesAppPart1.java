package day7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TheTreacheryOfWhalesAppPart1 {
	
//	static int iterations = 0;

	public static void main(String[] args) {
		try {
			File file = new File("src\\main\\java\\day7\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			List<Integer> positions = Arrays.stream(br.readLine().split(","))
					.map(Integer::parseInt)
					.collect(Collectors.toList());
			fr.close();

			int low = Collections.min(positions);
			int high = Collections.max(positions);

			while (low < high) {
				int mid = low  + ((high - low) / 2);
				
				int midFuel = fuel(positions, mid);
				int midPlusFuel = fuel(positions, mid+1);
				
				if(midFuel < midPlusFuel) {
					high = mid;
				} else {
					low = mid + 1;
				}
			}
			
			System.out.println("Position: " + low + " fuel " + fuel(positions, low));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	static public int fuel(List<Integer> positions, int toPosition) {
		int fuel = 0;
		for(Integer position : positions) {
			fuel += Math.abs(position - toPosition);
		}
		return fuel;
	}
}
