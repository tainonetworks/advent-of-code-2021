package day17;

public class TrickShotPart1 {
	
	static int versionsTotal = 0;
	
	static StringBuffer representation = new StringBuffer();

	public static void main(String[] args) {
//		try {
//			File file = new File("src\\main\\java\\day17\\input.txt");
//			FileReader fr = new FileReader(file);
//			BufferedReader br = new BufferedReader(fr);
//			String line = br.readLine();
			//need to parse the line
			int x1 = 153;
			int x2 = 199;
			//y's order is weird
			int y1 = -114;
			int y2 = -75;
			
			int yVelocity = Integer.max(Math.abs(y1), Math.abs(y2));
			if(Integer.max(y1, y2) < 0) {
				yVelocity--;
			}
			System.out.println("Max Y Veliciuty: " +(yVelocity+1)*yVelocity/2);
			
//			fr.close();			
//		} catch (IOException e) {
//			e.printStackTrace();
//		} 
	}
	
	
}
