package day18;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class SnailfishPart1 {
	
	private static SnailfishNumber rootNumber;

	public static void main(String[] args) {
		try {
			File file = new File("src\\main\\java\\day18\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			int count = 0;
			while ((line = br.readLine()) != null) {
				SnailfishNumber number = new SnailfishNumber(line);
				if(rootNumber == null) {
					rootNumber = number;
				} else {
					rootNumber = new SnailfishNumber(rootNumber, number);
				}
				System.out.println(rootNumber.toString());
				count++;
				if(count > 1) {
					while(rootNumber.reduce()) {
						System.out.println(rootNumber.toString());
					}
				}
			}
			fr.close();
			System.out.println(rootNumber.getMagnitude());
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
}
