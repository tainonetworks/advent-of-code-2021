package day18;

public class SnailfishNumber {

	SnailfishNumber parent;
	int value = -1;
	SnailfishNumber left;
	SnailfishNumber right;

	public SnailfishNumber() {}

	public SnailfishNumber(String number) {
		if(number.matches("-?\\d+?")) {
			this.value = Integer.parseInt(number);
		} else if(number.startsWith("[") && number.endsWith("]")) {
			number = number.substring(1, number.length() - 1);
			int count = 0;
			for(int i = 0; i < number.length(); i++) {
				char c = number.charAt(i);
				if(c == '[') {
					count++;
				} else if(c == ']') {
					count--;
				} else if(c == ',' && count == 0) {
					left = new SnailfishNumber(number.substring(0,i));
					left.parent = this;
					right = new SnailfishNumber(number.substring(i+1, number.length()));
					right.parent = this;
					break;
				}
			}
		} else {
			throw new IllegalArgumentException("not a Snailfish number");
		}
	}

	public SnailfishNumber(int number) {
		this.value = number;
	}

	public SnailfishNumber(SnailfishNumber left, SnailfishNumber right) {
		this.left = left;
		this.left.parent = this;
		this.right = right;
		this.right.parent = this;
	}

	public int getMagnitude() {
		int magnitude;
		if(value > -1) {
			magnitude = value;
		} else {
			magnitude = left.getMagnitude() * 3;
			magnitude += right.getMagnitude()  * 2;
		}
		return magnitude;
	}
	
	private boolean split() {
		if(value > 9) {
			double splittedValue = value / 2d;
			left = new SnailfishNumber((int)splittedValue);
			left.parent = this;
			right = new SnailfishNumber((int)Math.ceil(splittedValue));
			right.parent = this;
			value = -1;
			explode();
			return true;
		}
		return false;
	}

	private int getLevel() {
		int level = 1;
		if(parent != null) {
			level += parent.getLevel();
		}
		return level;
	}
	
	private void addBackward(int leftNumber, int rightNumber) {
		if(parent == null || leftNumber == 0 && rightNumber == 0) {
			return;
		}
		if(parent.left == this) {
			if(parent.right.value > -1) {
				parent.right.value += rightNumber;
			} else {
				addForward(parent.right, rightNumber, 0);
			}
			rightNumber = 0;
		} else {
			if(parent.left.value > -1) {
				parent.left.value += leftNumber;
			} else {
				addForward(parent.left, 0, leftNumber);
			}
			leftNumber = 0;
		}
		parent.addBackward(leftNumber, rightNumber);
		
	}
	
	private void addForward(SnailfishNumber pair, int leftNumber, int rightNumber) {
		if(leftNumber != 0 && pair.left != null) {
			if(pair.left.value > -1) {
				pair.left.value += leftNumber;
			} else {
				addForward(pair.left, leftNumber, rightNumber);
			}
		} else if(rightNumber != 0 && pair.right != null){
			if(pair.right.value > -1) {
				pair.right.value += rightNumber;
			} else {
				addForward(pair.right, leftNumber, rightNumber);
			}
		}
	}

	private boolean explode() {
		if(left != null && left.value > -1 && left.value <= 9 && right != null && right.value > -1 && right.value <= 9 && getLevel() > 4) {
			addBackward(left.value, right.value);
			left.parent = null;
			left = null;
			right.parent = null;
			right = null;
			value = 0;
			return true;
		}
		return false;
	}

	public boolean reduce() {		
		return left != null && left.reduce() || split() || explode() || right != null && right.reduce();
	}
	
	
	@Override
	public String toString() {
		if(value > -1) {
			return String.valueOf(value);
		} else {
			return "[" + left.toString() + "," + right.toString() + "]";
		}
	}
	//[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]
}
 