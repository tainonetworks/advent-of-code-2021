package day6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LanternfishAppPart2 {

	public static void main(String[] args) {
		Map<Integer, Long> buckets = new HashMap<Integer, Long>();
		buckets.put(0, 0L);
		buckets.put(1, 0L);
		buckets.put(2, 0L);
		buckets.put(3, 0L);
		buckets.put(4, 0L);
		buckets.put(5, 0L);
		buckets.put(6, 0L);
		buckets.put(7, 0L);
		buckets.put(8, 0L);
		int days = 256;
		try {
			File file = new File("src\\main\\java\\day6\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			List<Integer> fishes = Arrays.stream(br.readLine().split(","))
			        .map(Integer::parseInt)
			        .collect(Collectors.toList());
			fr.close();
			
			for(int fish : fishes) {
				long amount = buckets.get(fish);
				buckets.put(fish, ++amount);
			}
			
			for(int i = 0; i < days; i++) {
				long reset = buckets.get(0);
				buckets.put(0, buckets.get(1));
				buckets.put(1, buckets.get(2));
				buckets.put(2, buckets.get(3));
				buckets.put(3, buckets.get(4));
				buckets.put(4, buckets.get(5));
				buckets.put(5, buckets.get(6));
				buckets.put(6, buckets.get(7) + reset);
				buckets.put(7, buckets.get(8));
				buckets.put(8, reset);
			}
			
			long total = 0;
			for(long value : buckets.values()) {
				System.out.println(value);
				total += value;
			}
			
			System.out.println(total);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
