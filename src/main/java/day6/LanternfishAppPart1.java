package day6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LanternfishAppPart1 {

	public static void main(String[] args) {
		int days = 80;
		try {
			File file = new File("src\\main\\java\\day6\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			List<Integer> fishes = Arrays.stream(br.readLine().split(","))
			        .map(Integer::parseInt)
			        .collect(Collectors.toList());
			fr.close();
			
			for(int i = 0; i < days; i++) {
				List<Integer> newBorns = new ArrayList<Integer>();
				for(int j = 0; j < fishes.size(); j++) {
					int fish = fishes.get(j);
					if(fish == 0) {
						fish = 6;
						newBorns.add(8);
					} else {
						fish = fish - 1;
					}
					fishes.set(j, fish);
				}
				fishes.addAll(newBorns);
			}
			
			System.out.print(fishes.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
