package day10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class SyntaxScoringPart2 {

	public static void main(String[] args) {
		List<Long> scores = new ArrayList<Long>();
		try {
			File file = new File("src\\main\\java\\day10\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			primary:
			while ((line = br.readLine()) != null) {
				Stack<Character> stack = new Stack<Character>();
				for(char c : line.toCharArray()) {
					if(c == '(' || c == '[' || c == '{' || c == '<') {
						stack.push(c);
					} else {
						char l = stack.pop();
						if(c == ')' && l != '(') {
							continue primary;
						} else if(c == ']' && l != '[') {
							continue primary;
						} else if(c == '}' && l != '{') {
							continue primary;
						} else if(c == '>' && l != '<') {
							continue primary;
						}
					}
				}
				long score = 0L;
				while(!stack.isEmpty()) {
					char c = stack.pop();
					score *= 5;
					if(c == '(') {
						score += 1;
					} else if(c == '[') {
						score += 2;
					} else if(c == '{') {
						score += 3;
					} else if(c == '<') {
						score += 4;
					}
				}
				scores.add(score);
			}
			fr.close();
			System.out.println(scores.get((scores.size())/2));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
