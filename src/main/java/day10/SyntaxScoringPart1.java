package day10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;

public class SyntaxScoringPart1 {

	public static void main(String[] args) {
		int score = 0;
		try {
			File file = new File("src\\main\\java\\day10\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			primary:
			while ((line = br.readLine()) != null) {
				Stack<Character> stack = new Stack<Character>();
				for(char c : line.toCharArray()) {
					if(c == '(' || c == '[' || c == '{' || c == '<') {
						stack.push(c);
					} else {
						char l = stack.pop();
						if(c == ')' && l != '(') {
							score += 3;
							continue primary;
						} else if(c == ']' && l != '[') {
							score += 57;
							continue primary;
						} else if(c == '}' && l != '{') {
							score += 1197;
							continue primary;
						} else if(c == '>' && l != '<') {
							score += 25137;
							continue primary;
						}
					}
				}
			}
			fr.close();
			System.out.println(score);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
