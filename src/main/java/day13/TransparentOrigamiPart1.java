package day13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TransparentOrigamiPart1 {
	
	public static void main(String[] args) {
		Set<String> points = new HashSet<String>();
		try {
			File file = new File("src\\main\\java\\day13\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				if(line.isEmpty()) {
					continue;
				} else if(line.indexOf(',') > 0) {
					points.add(line);
				} else {
					int fold = Integer.valueOf(line.substring(line.indexOf("=") + 1, line.length()));
					List<String> toDelete = new ArrayList<String>();
					List<String> toAdd = new ArrayList<String>();
					if(line.startsWith("fold along x=")) {
						for(String point : points) {
							String[] parts = point.split(",");
							int value = Integer.valueOf(parts[0]);
							if(value >= fold) {
								toDelete.add(point);
								if(value > fold) {
									toAdd.add((value - (value - fold) * 2) + "," + parts[1]);
								}
							}
						}
					} else {//if(line.startsWith("fold along y=")) {
						for(String point : points) {
							String[] parts = point.split(",");
							int value = Integer.valueOf(parts[1]);
							if(value >= fold) {
								toDelete.add(point);
								if(value > fold) {
									toAdd.add(parts[0] + "," + (value - (value - fold) * 2));
								}
							}
						}
					}
					points.removeAll(toDelete);
					points.addAll(toAdd);
					break;
				}
			}
			fr.close();
			System.out.println(points.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
