package day12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PassagePathingPart2 {

	static Map<String, Set<String>> nodes = new HashMap<String, Set<String>>();
	static int ended = 0;

	public static void main(String[] args) {
		try {
			File file = new File("src\\main\\java\\day12\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] path = line.split("-");
				addEdge(path[0], path[1]);
			}

			calculateRoutes("start", new HashSet<String>(), false);

			System.out.println(ended);
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//3509

	static void addEdge(String startNode, String endNode) {
		if(startNode.equals("end") || endNode.equals("start")) {
			String temp = startNode;
			startNode = endNode;
			endNode = temp;
		}

		Set<String> endNodes = nodes.get(startNode);
		if(endNodes == null) {
			endNodes = new HashSet<String>();
			nodes.put(startNode, endNodes);
		}
		endNodes.add(endNode);

		if(!startNode.equals("start") && !endNode.equals("end")) {
			endNodes = nodes.get(endNode);
			if(endNodes == null) {
				endNodes = new HashSet<String>();
				nodes.put(endNode, endNodes);
			}
			endNodes.add(startNode);
		}
	}

	//I will assume that they will avoid infinite loops between adjacent UPPERCASE nodes
	static void calculateRoutes(String node, Set<String> restrictedNodes, boolean doubled) {
		if(node.equals("end")) {
			ended++;
		} else {
			Set<String> nextNodes = new HashSet<String>(nodes.get(node));
			
			if(node.equals(node.toLowerCase())) {
				restrictedNodes.add(node);
			}

			for(String nextNode : nextNodes) {
				if(restrictedNodes.contains(nextNode)) {
					if(!doubled) {
						calculateRoutes(nextNode, new HashSet<String>(restrictedNodes), true);
					}
				} else {
					calculateRoutes(nextNode, new HashSet<String>(restrictedNodes), doubled);
				}
			}
		}
	}
}
