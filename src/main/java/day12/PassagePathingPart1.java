package day12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PassagePathingPart1 {

	static Map<String, List<String>> nodes = new HashMap<String, List<String>>();
	static int ended = 0;
	
	public static void main(String[] args) {
		try {
			File file = new File("src\\main\\java\\day12\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] path = line.split("-");
				addEdge(path[0], path[1]);
			}
			
			calculateRoutes("start", new ArrayList<String>());
			
			System.out.println(ended);
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static void addEdge(String startNode, String endNode) {
		if(startNode.equals("end") || endNode.equals("start")) {
			String temp = startNode;
			startNode = endNode;
			endNode = temp;
		}
		
		List<String> endNodes = nodes.get(startNode);
		if(endNodes == null) {
			endNodes = new ArrayList<String>();
			nodes.put(startNode, endNodes);
		}
		endNodes.add(endNode);
		
		if(!startNode.equals("start") && !endNode.equals("end")) {
			endNodes = nodes.get(endNode);
			if(endNodes == null) {
				endNodes = new ArrayList<String>();
				nodes.put(endNode, endNodes);
			}
			endNodes.add(startNode);
		}
	}
	
	//I will assume that they will avoid infinite loops between adjacent UPPERCASE nodes
	static void calculateRoutes(String node, List<String> restrictedNodes) {
		if(node.equals("end")) {
			ended++;
		} else {
			
			List<String> nextNodes = new ArrayList<String>(nodes.get(node));
			nextNodes.removeAll(restrictedNodes);
	
			if(node.equals(node.toLowerCase())) {
				restrictedNodes.add(node);
			}
			
			for(String nextNode : nextNodes) {
				calculateRoutes(nextNode, new ArrayList<String>(restrictedNodes));
			}
		}
	}
}
