package day14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
//import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExtendedPolymerizationPart2 {
	
	static Map<String, String> rules = new HashMap<String, String>();
	static Map<Integer, Map<String, Map<String, Long>>> stepsAmounts = new HashMap<Integer, Map<String, Map<String, Long>>>();
	
	public static void main(String[] args) {
		List<String> pairs = new ArrayList<String>();
		Map<String, Long> totalAmounts = new HashMap<String, Long>();
		try {
			File file = new File("src\\main\\java\\day14\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				if(line.isEmpty()) {
					continue;
				} else if(line.indexOf('-') > 0) {
					String[] parts = line.split(" -> ");
					rules.put(parts[0], parts[1]);
				} else {
					for(int i = 0; i < line.length() ; i++) {
						addAmount(line.substring(i, i+1), 1L, totalAmounts);
					}
					
					for(int i = 0; i < line.length() - 1 ; i++) {
						pairs.add(line.substring(i, i+2));
					}
				}
			}
			fr.close();
			
			for(String pair : pairs) {
				addAmounts(process(pair, 40), totalAmounts);
			}
			
			System.out.println(Collections.max(totalAmounts.values()) - Collections.min(totalAmounts.values()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static Map<String, Long> process(String pair, int step) {
		Map<String, Long> amounts = new HashMap<String, Long>();
		if(step > 0) {
			String insert = rules.get(pair);
			addAmount(insert, 1L, amounts);
			
			Map<String, Map<String, Long>> stepAmounts = getStepAmounts(step);
			
			String childPair = pair.substring(0,1) + insert;
			if(stepAmounts.containsKey(childPair)) {
				addAmounts(stepAmounts.get(childPair), amounts);
			} else {
				Map<String, Long> pairAmounts = process(childPair, step - 1);
				stepAmounts.put(childPair, pairAmounts);
				addAmounts(pairAmounts, amounts);
			}
			
			childPair = insert + pair.substring(1);
			if(stepAmounts.containsKey(childPair)) {
				addAmounts(stepAmounts.get(childPair), amounts);
			} else {
				Map<String, Long> pairAmounts = process(childPair, step - 1);
				stepAmounts.put(childPair, pairAmounts);
				addAmounts(pairAmounts, amounts);
			}
		}
		return amounts;
	}
	
	static void addAmount(String key, Long amount, Map<String, Long> toMap) {
		Long existingAmount = toMap.get(key);
		if(existingAmount == null) {
			existingAmount = 0L;
		}
		toMap.put(key, existingAmount + amount);
	}
	
	static void addAmounts(Map<String, Long> fromMap, Map<String, Long> toMap) {
		for(String key : fromMap.keySet()) {
			addAmount(key, fromMap.get(key), toMap);
		}
	}
	
	static Map<String, Map<String, Long>> getStepAmounts(int step) {
		Map<String, Map<String, Long>> stepAmounts = stepsAmounts.get(step);
		if(stepAmounts == null) {
			stepAmounts = new HashMap<String, Map<String, Long>>(); 
			stepsAmounts.put(step, stepAmounts);
		}
		return stepAmounts;
	}
}
