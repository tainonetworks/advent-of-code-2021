package day14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ExtendedPolymerizationPart1 {
	
	public static void main(String[] args) {
		StringBuffer template = new StringBuffer();
		Map<String, String> rules = new HashMap<String, String>();
		Map<Character, Long> counts = new HashMap<Character, Long>();
		try {
			File file = new File("src\\main\\java\\day14\\input.txt");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				if(line.isEmpty()) {
					continue;
				} else if(line.indexOf('-') > 0) {
					String[] parts = line.split(" -> ");
					rules.put(parts[0], parts[1]);
				} else {
					template.append(line);
				}
			}
			fr.close();
			
			for(int i = 0; i < 10; i++) {
				for(int j = 0; j < template.length() - 1 ; j++) {
					for(String regex : rules.keySet()) {
						if(template.toString().startsWith(regex, j)) {
							String insert = rules.get(regex);
							template = template.insert(++j, insert);
							break;
						}
					}
				}
			}
			
			for(int i = 0; i < template.length(); i++) {
				char c = template.charAt(i);
				Long count = counts.get(c);
				if(count == null) {
					count = 0L;
				}
				counts.put(c, ++count);
			}

			Collection<Long> values = counts.values();
			System.out.println(Collections.max(values) - Collections.min(values));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
